<?php
	$title='添加广告';
    require ('./header.php');
?>

<div class="mdui-container" style="margin-top: 4%;">
	<div class="panel panel-default">
		<div class="panel-heading"><b><?php echo $lang->admin->add_ads;?></b></div>
        <div class="panel-body">
            <div class="alert alert-info" role="alert">
                <center>温馨提示：序号用来排序[数字越大排名越前]，别名是指链接别名[不可重复]</center>
            </div>
            <form action="./submit.php" method="post" enctype="multipart/form-data">
                <input type="text" value="ads" name="from" style="display: none;">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">标题</span>
                    <input type="text" class="form-control" placeholder="请输入广告标题[必填]" aria-describedby="basic-addon1" name="title" required>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">图片</span>
                    <input type="file" name="image" class="form-control">
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">类型</span>
                    <select name="type" class="form-control" required>
                        <option value="">请选择广告类型分类[必选]</option>
                        <option value="1">横幅[必选]</option>
                        <option value="2">对联[必选]</option>

                    </select>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">链接</span>
                    <input type="url" class="form-control" placeholder="请输入广告链接[必填]" aria-describedby="basic-addon1" name="url">
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">排序</span>
                    <input type="number" class="form-control" placeholder="请输入站点序号[必填，且只能填数字]" aria-describedby="basic-addon1" name="sort" >
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">状态</span>
                    <div class="radio " style="padding-left:15px;">
                        <label><input type="radio" aria-describedby="basic-addon1" name="status" value="0"/>下架</label>
                        <label><input type="radio"  aria-describedby="basic-addon1" name="status" value="1"/>上架</label>
                    </div>
                </div>
                <br>
                <br>
                <div style="text-align: center;">
                    <input type="submit" class="btn btn-info" style="width: 80%;" value="添加">
                </div>
            </form>
        </div>
	</div>
</div>

<?php
    require ('./footer.php');
?>