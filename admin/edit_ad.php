<?php
header("Content-Type:text/html;charset=utf-8");
	$title='修改站点信息';
    require ('./header.php');
?>

<div class="mdui-container" style="margin-top: 4%;">
	<div class="panel panel-default">
		<div class="panel-heading"><b><?php echo $lang->admin->edit_ad;?></b></div>
		<div class="panel-body">
			<div class="alert alert-info" role="alert">
				<center>温馨提示：序号用来排序[数字越大排名越前]，别名是指链接别名[不可重复]</center>
			</div> 
			<form action="./submit.php" method="post" enctype="multipart/form-data">
				<?php
					mysql_query("set names utf8");
					$id = $_GET['id'];
					$sql = "select * from zzdh_ads where Id = '".$id."'";//查询数据库
					$result = mysql_query($sql);
					while($row = mysql_fetch_array($result))
				{	
				?>
				<input type="text" value="edit_ad" name="from" style="display: none;">
				<input type="text" value="<?php echo $id;?>" name="id" style="display: none;">
                    <input type="text" value="<?php echo $row['image'];?>" name="image" style="display: none;">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">标题</span>
                    <input  type="text" class="form-control" placeholder="请输入广告标题[必填]" aria-describedby="basic-addon1" name="title" value="<?php echo $row['title'];?>" >
                </div>
            <br>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">图片</span>
                        <div style="padding-left:15px;"><input type="file" name="image"/><span style="color:#ff2233">不替换图片请忽略</span></div>

                    </div>
            <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">类型</span>
                    <select name="type" class="form-control" required>
                        <option value="">请选择广告类型分类[必选]</option>
                        <option value="1" <?php if($row['type'] ==1){echo "selected='selected'";} ?>>横幅</option>
                        <option value="2" <?php if($row['type'] ==2){echo "selected='selected'";} ?>>对联</option>

                    </select>
                </div>
            <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">链接</span>
                    <input type="url" class="form-control" placeholder="请输入广告链接[必填]" aria-describedby="basic-addon1" name="url" value="<?php echo $row['url'];?>">
                </div>
            <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">排序</span>
                    <input type="number" class="form-control" placeholder="请输入站点序号[必填，且只能填数字]" aria-describedby="basic-addon1" name="sort" value="<?php echo $row['sort'];?>">
                </div>
            <br>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">状态</span>
                    <div class="radio" style="margin-left:15px;">
                        <label><input type="radio" aria-describedby="basic-addon1" name="status" value="0" <?php if($row['status'] ==0){echo "checked='checked'";} ?>/>下架</label>
                        <label><input type="radio"  aria-describedby="basic-addon1" name="status" value="1" <?php if($row['status'] ==1){echo "checked='checked'";} ?>/>上架</label>
                    </div>
                </div>
            <br>
				<div style="text-align: center;">
					<input type="submit" class="btn btn-info" style="width: 80%;" value="修改">
			    </div>
				<?php }?>
			</form>
		</div>
	</div>
</div>

<?php
    require ('./footer.php');
?>