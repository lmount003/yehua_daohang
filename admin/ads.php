<?php
$title='广告列表';
require ('./header.php');
?>

    <div class="mdui-container" style="margin-top: 4%;">
        <div class="panel panel-default">
            <div class="panel-heading"><b><?php echo $lang->admin->get_ads;?></b></div>
            <div class="panel-body">
                <div class="alert alert-info" role="alert">
                    <center>共 <b><?php echo $cntads;?></b> 个广告 ==》<a href="add_ad.php">添加广告</a></center>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr style="white-space: nowrap;">
                            <th class="text-center" style="width: 10%;">名称</th>
                            <th class="text-center" style="width: 25%;">图片</th>
                            <th class="text-center" style="width: 10%;">类型</th>
                            <th class="text-center" style="width: 15%;">链接</th>
                            <th class="text-center" style="width: 5%;">状态</th>
                            <th class="text-center" style="width: 5%;">排序</th>
                            <th class="text-center" style="width: 10%;">添加时间</th>
                            <th class="text-center" style="width: 15%;">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $pagesize=10;
                        if (!isset($_GET['page'])) {
                            $page = 1;
                            $pageu = $page - 1;
                        } else {
                            $page = $_GET['page'];
                            $pageu = ($page - 1) * $pagesize;
                        }
                        mysql_query("set names utf8");
                        $result = mysql_query("SELECT * FROM zzdh_ads order  by sort,id desc limit $pageu,$pagesize");
                        while($row = mysql_fetch_array($result))
                        {
                            ?>
                            <tr class="text-center">
                                <td><?php echo $row['title'];?></td>
                                <td><img src="<?php echo '/'.$row['image'];?>"  style="width:40%;"></td>
                                <td><?php if($row['type']==1){echo "横幅";}else{echo "对联";};?></td>
                                <td><a href="/go.php?url=<?php echo $row['url'];?>" title="<?php echo $row['url'];?>" target="_blank"><?php echo $row['url'];?></a></td>
                                <td><?php if($row['status']==1){echo "<font color=orange>上架</font>";}else{echo "下架";};?></td>

                                <td><?php echo $row['sort'];?></td>
                                <td><?php echo date('Y-m-d H:i:s', $row['add_time']);
                                    ?></td>
                                <td>
                                    <a href="./edit_ad.php?id=<?php echo $row['id'];?>" class="btn btn-xs btn-info">修改</a>
                                    <a href="./del.php?from=ads&id=<?php echo $row['id'];?>" class="btn btn-xs btn-danger" onclick="javascript:return confirm('您确定要删除名称为「<?php echo $row['name'];?>」的站点吗？')">删除</a>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
                <center>
                    <?php
                    echo'<ul class="pagination">';
                    $s = ceil($cntads / $pagesize);
                    $first=1;
                    $prev=$page-1;
                    $next=$page+1;
                    $last=$s;
                    if ($page>1) {
                        echo '<li><a href="./list.php?page='.$first.$link.'">首页</a></li>';
                        echo '<li><a href="./list.php?page='.$prev.$link.'">&laquo;</a></li>';
                    } else {
                        echo '<li class="disabled"><a>首页</a></li>';
                        echo '<li class="disabled"><a>&laquo;</a></li>';
                    }
                    for ($i=1;$i<$page;$i++)
                        echo '<li><a href="./list.php?page='.$i.$link.'">'.$i .'</a></li>';
                    echo '<li class="disabled"><a>'.$page.'</a></li>';
                    for ($i=$page+1;$i<=$s;$i++)
                        echo '<li><a href="./list.php?page='.$i.$link.'">'.$i .'</a></li>';
                    echo '';
                    if ($page<$s) {
                        echo '<li><a href="./list.php?page='.$next.$link.'">&raquo;</a></li>';
                        echo '<li><a href="./list.php?page='.$last.$link.'">尾页</a></li>';
                    } else {
                        echo '<li class="disabled"><a>&raquo;</a></li>';
                        echo '<li class="disabled"><a>尾页</a></li>';
                    }
                    echo'</ul>';
                    ?>
                </center>
            </div>
        </div>
    </div>

<?php
require ('./footer.php');
?>